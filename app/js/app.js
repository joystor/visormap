var App = {};

App.imgPath = "http://joystor.xyz/media/";
App.imgExt = ".png";

(function() {
  'use strict';

  App.init = function() {
    this.gmap = new App.Map();
    $('#app').append(this.gmap.render().$el);
    this.gmap.activate();

    this.menu = new App.Menu();
    $('#app').append(this.menu.render().$el);
    this.menu.activate();

    this.RUTA = {};
  };

  $(function() {
    App.init();
  });

})();

(function() {
  'use strict';

  /*
  3 check capas
  lineas naranja
  pines azul
  recorridos -pines a visitar azul
  lecturas - pines color verde e incidencias rojas
  rastreo - ruta naranja
  */

  document.addEventListener('getData',function(e){
    var prms = e.data.split(';');
    var clbk = prms[1];
    var tbl = prms[0].split('/');
    var data = "[]";

    switch (tbl[0]) {
      case 'rutas':
        data = '[{"id":"1","descripcion":"GU0101","fecha":"11/11/2016 12:00:00 a.m."},{"id":"2","descripcion":"GU0102","fecha":"12/11/2016 12:00:00 a.m."},{"id":"3","descripcion":"GU0103","fecha":"13/11/2016 12:00:00 a.m."},{"id":"4","descripcion":"GU0104","fecha":"14/11/2016 12:00:00 a.m."},{"id":"5","descripcion":"GU0105","fecha":"15/11/2016 12:00:00 a.m."},{"id":"6","descripcion":"GU0106","fecha":"16/11/2016 12:00:00 a.m."},{"id":"7","descripcion":"GU0107","fecha":"17/11/2016 12:00:00 a.m."}]';
        break;
      case 'recorridosInfo_org':
        data = '{ '+ //6 5 2 3 4 7
'"recorridos":[ '+  //planeado r_posicion_xy
'  {"r_posicion_xy":"-99.194156,19.536871","lectura_id":"1","fecha":"22/10/2016 12:25","tipo":"1","folio_l":"2","lectura":"15002","incidencia":"","observaciones":"Observacion 1","l_posicion_xy":"-99.194156,19.536871","nombre_img":"nombre img 1","ruta":"imagen/01.png","predio_id":"1","folio_p":"2","lectura_anterior":"1200","tipo_servicio":"1","direccion":"dir 1","telefono":"000 000000","nombre":"juan","apellidos":"peres"}, '+
'  {"r_posicion_xy":"-99.194107,19.537255","lectura_id":"2","fecha":"22/10/2016 13:25","tipo":"2","folio_l":"3","lectura":"15245","incidencia":"Una incidencia 1","observaciones":"","l_posicion_xy":"-99.194107,19.537255","nombre_img":"nombre img 2","ruta":"imagen/01.png","predio_id":"2","folio_p":"3","lectura_anterior":"1200","tipo_servicio":"1","direccion":"dir2","telefono":"000 000000","nombre":"asd","apellidos":"ape"}, '+
'  {"r_posicion_xy":"-99.194071,19.537811","lectura_id":"3","fecha":"22/10/2016 14:25","tipo":"1","folio_l":"4","lectura":"15445","incidencia":"","observaciones":"Observacion 2","l_posicion_xy":"-99.194071,19.537811","nombre_img":"nombre img 3","ruta":"imagen/01.png","predio_id":"3","folio_p":"4","lectura_anterior":"1200","tipo_servicio":"2","direccion":"dir3","telefono":"000 000000","nombre":"qwerty","apellidos":"ape"}, '+
'  {"r_posicion_xy":"-99.193927,19.538029","lectura_id":"4","fecha":"22/10/2016 15:25","tipo":"2","folio_l":"5","lectura":"15645","incidencia":"Una incidencia 2","observaciones":"","l_posicion_xy":"-99.193927,19.538029","nombre_img":"nombre img 4","ruta":"imagen/01.png","predio_id":"4","folio_p":"5","lectura_anterior":"1200","tipo_servicio":"2","direccion":"dir4","telefono":"000 000000","nombre":"zxcvb","apellidos":"ape"}, '+
'  {"r_posicion_xy":"-99.193066,19.537901","lectura_id":"5","fecha":"22/10/2016 16:25","tipo":"1","folio_l":"6","lectura":"15845","incidencia":"","observaciones":"Observacion 3","l_posicion_xy":"-99.193066,19.537901","nombre_img":"nombre img 5","ruta":"imagen/01.png","predio_id":"5","folio_p":"6","lectura_anterior":"1200","tipo_servicio":"1","direccion":"dir5","telefono":"000 000000","nombre":"rtiy","apellidos":"ape"}, '+
'  {"r_posicion_xy":"-99.192792,19.539110","lectura_id":"6","fecha":"22/10/2016 17:25","tipo":"2","folio_l":"7","lectura":"15945","incidencia":"Una incidencia 3","observaciones":"","l_posicion_xy":"-99.192792,19.539110","nombre_img":"nombre img 6","ruta":"imagen/01.png","predio_id":"6","folio_p":"7","lectura_anterior":"1200","tipo_servicio":"1","direccion":"dir6","telefono":"000 000000","nombre":"uiopiup","apellidos":"ape"} '+
'], '+
'"rastreos":[ '+
'  {"id":"1","fecha":"22/10/2016 12:25","posicion_xy":"-99.194156,19.536871","rutas_id":"1"}, '+
'  {"id":"2","fecha":"22/10/2016 13:25","posicion_xy":"-99.194107,19.537255","rutas_id":"1"}, '+
'  {"id":"3","fecha":"22/10/2016 14:25","posicion_xy":"-99.194071,19.537811","rutas_id":"1"}, '+
'  {"id":"4","fecha":"22/10/2016 15:25","posicion_xy":"-99.193927,19.538029","rutas_id":"1"}, '+
'  {"id":"5","fecha":"22/10/2016 16:25","posicion_xy":"-99.193066,19.537901","rutas_id":"1"}, '+
'  {"id":"6","fecha":"22/10/2016 17:25","posicion_xy":"-99.192792,19.539110","rutas_id":"1"} '+
'], '+
'"equipos":[ '+
'  {"id":"","modelo":"","serie":"","descripcion":"","rutas_id":"","fecha_creacion":"","fecha_actualizacion":""} '+
']}';
        break;
      case 'recorridosInfo':
        var id = tbl[1].split('=')[1];
        switch(id){
          case '1':
          data = '{"recorridos":[{"r_posicion_xy":"19.5389407,-99.1928041","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img001","ruta":"img/lecturas","predio_id":"2","folio_p":"2","lectura_anterior":"15002","tipo_servicio":"True","direccion":"CARRETERA QUERETARO-SAN LUIS POTOSI No. 12401, Edif. ANTEA LIFESTYLE CENTER, Col. EJIDO EL SALITRE","telefono":"55-36-07-86","nombre":"F/1446","apellidos":"IXE BANCO, S.A."},{"r_posicion_xy":"19.5389407,-99.1928041","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img002","ruta":"img/lecturas","predio_id":"2","folio_p":"2","lectura_anterior":"15002","tipo_servicio":"True","direccion":"CARRETERA QUERETARO-SAN LUIS POTOSI No. 12401, Edif. ANTEA LIFESTYLE CENTER, Col. EJIDO EL SALITRE","telefono":"55-36-07-86","nombre":"F/1446","apellidos":"IXE BANCO, S.A."},{"r_posicion_xy":"19.5390722,-99.1928128","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"3","folio_p":"3","lectura_anterior":"15003","tipo_servicio":"True","direccion":"CIRCUITO BALCONES No. 228, Mz. 04, Col. BALCONES","telefono":"55-36-07-86","nombre":"MARIA MONICA","apellidos":"NUÑEZ RUBIO"},{"r_posicion_xy":"19.5392706,-99.1928108","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"4","folio_p":"4","lectura_anterior":"15004","tipo_servicio":"True","direccion":"ANILLO VIAL II \'FRAY JUNIPERO SERRA\' No. 2501, Mz. 02, Int. 07, Edif. EL RESPIRO, Col. SANTA FE","telefono":"55-36-07-86","nombre":"YOLANDA AIDA","apellidos":"CEJA LOPEZ"},{"r_posicion_xy":"19.5394791,-99.1927725","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"5","folio_p":"5","lectura_anterior":"15005","tipo_servicio":"True","direccion":"AV. SANTA ROSA No. 5201, Mz. 03, Int. 02, Edif. AURA, Col. VALLE DE JURIQUILLA","telefono":"55-36-07-86","nombre":"JUAN MANUEL","apellidos":"GONZALEZ RODRIGUEZ"},{"r_posicion_xy":"19.5394918,-99.192617","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"6","folio_p":"6","lectura_anterior":"15006","tipo_servicio":"True","direccion":"ANILLO VIAL II \'FRAY JUNIPERO SERRA\' No. 2501, Mz. 02, Int. 53, Edif. EL RESPIRO, Col. SANTA FE","telefono":"55-36-07-86","nombre":"GERARDO","apellidos":"MARTINEZ CERVANTES"},{"r_posicion_xy":"19.5394785,-99.1924916","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"7","folio_p":"7","lectura_anterior":"15007","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5393256,-99.1923561","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"8","folio_p":"8","lectura_anterior":"15008","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "}],"rastreos":[{"id":"1","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5393256,-99.1923561","rutas_id":"1"},{"id":"2","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5393559,-99.1923186","rutas_id":"1"},{"id":"3","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5394791,-99.1923367","rutas_id":"1"},{"id":"4","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5394943,-99.1924306","rutas_id":"1"},{"id":"5","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5395,-99.1925137","rutas_id":"1"},{"id":"6","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5395069,-99.1925962","rutas_id":"1"}],"equipos":[{"id":"1","modelo":"001","serie":"A001","descripcion":"Test 001","rutas_id":"1","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '2':
          data = '{"recorridos":[{"r_posicion_xy":"19.5418306,-99.19601","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"9","folio_p":"9","lectura_anterior":"15009","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5418306,-99.1955835","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"10","folio_p":"10","lectura_anterior":"15010","tipo_servicio":"True","direccion":"SANTA  ANA No. 116, Mz. 05, Col. VALLE DE JURIQUILLA","telefono":"55-36-07-86","nombre":"FERNANDO","apellidos":"RIVERA MENDOZA"},{"r_posicion_xy":"19.5418331,-99.1951999","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"11","folio_p":"11","lectura_anterior":"15011","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5418154,-99.1947279","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"12","folio_p":"12","lectura_anterior":"15012","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5417522,-99.1943685","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"13","folio_p":"13","lectura_anterior":"15013","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5417547,-99.1940949","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"14","folio_p":"14","lectura_anterior":"15014","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5417598,-99.1937247","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"15","folio_p":"15","lectura_anterior":"15015","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5417547,-99.1934914","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img001","ruta":"img/lecturas","predio_id":"16","folio_p":"16","lectura_anterior":"15016","tipo_servicio":"True","direccion":"CIRCUITO BALCONES No. 164, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"MANUEL GERARDO","apellidos":"GARCIA RANGEL"},{"r_posicion_xy":"19.5417547,-99.1934914","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img002","ruta":"img/lecturas","predio_id":"16","folio_p":"16","lectura_anterior":"15016","tipo_servicio":"True","direccion":"CIRCUITO BALCONES No. 164, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"MANUEL GERARDO","apellidos":"GARCIA RANGEL"}],"rastreos":[],"equipos":[{"id":"2","modelo":"001","serie":"A002","descripcion":"Test 002","rutas_id":"2","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '3':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"3","modelo":"001","serie":"A003","descripcion":"Test 003","rutas_id":"3","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '4':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"4","modelo":"001","serie":"A004","descripcion":"Test 004","rutas_id":"4","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '5':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"5","modelo":"002","serie":"B005","descripcion":"Test 005","rutas_id":"5","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '6':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"6","modelo":"002","serie":"B006","descripcion":"Test 006 ","rutas_id":"6","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '7':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"7","modelo":"002","serie":"B007","descripcion":"Test 007","rutas_id":"7","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
        }
        //data = '{"recorridos":[{"r_posicion_xy":"19.5389407,-99.1928041","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img001","ruta":"img/lecturas","predio_id":"2","folio_p":"2","lectura_anterior":"15002","tipo_servicio":"True","direccion":"CARRETERA QUERETARO-SAN LUIS POTOSI No. 12401, Edif. ANTEA LIFESTYLE CENTER, Col. EJIDO EL SALITRE","telefono":"55-36-07-86","nombre":"F/1446","apellidos":"IXE BANCO, S.A."},{"r_posicion_xy":"19.5389407,-99.1928041","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img002","ruta":"img/lecturas","predio_id":"2","folio_p":"2","lectura_anterior":"15002","tipo_servicio":"True","direccion":"CARRETERA QUERETARO-SAN LUIS POTOSI No. 12401, Edif. ANTEA LIFESTYLE CENTER, Col. EJIDO EL SALITRE","telefono":"55-36-07-86","nombre":"F/1446","apellidos":"IXE BANCO, S.A."},{"r_posicion_xy":"19.5390722,-99.1928128","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"3","folio_p":"3","lectura_anterior":"15003","tipo_servicio":"True","direccion":"CIRCUITO BALCONES No. 228, Mz. 04, Col. BALCONES","telefono":"55-36-07-86","nombre":"MARIA MONICA","apellidos":"NUÑEZ RUBIO"},{"r_posicion_xy":"19.5392706,-99.1928108","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"4","folio_p":"4","lectura_anterior":"15004","tipo_servicio":"True","direccion":"ANILLO VIAL II \'FRAY JUNIPERO SERRA\' No. 2501, Mz. 02, Int. 07, Edif. EL RESPIRO, Col. SANTA FE","telefono":"55-36-07-86","nombre":"YOLANDA AIDA","apellidos":"CEJA LOPEZ"},{"r_posicion_xy":"19.5394791,-99.1927725","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"5","folio_p":"5","lectura_anterior":"15005","tipo_servicio":"True","direccion":"AV. SANTA ROSA No. 5201, Mz. 03, Int. 02, Edif. AURA, Col. VALLE DE JURIQUILLA","telefono":"55-36-07-86","nombre":"JUAN MANUEL","apellidos":"GONZALEZ RODRIGUEZ"},{"r_posicion_xy":"19.5394918,-99.192617","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"6","folio_p":"6","lectura_anterior":"15006","tipo_servicio":"True","direccion":"ANILLO VIAL II \'FRAY JUNIPERO SERRA\' No. 2501, Mz. 02, Int. 53, Edif. EL RESPIRO, Col. SANTA FE","telefono":"55-36-07-86","nombre":"GERARDO","apellidos":"MARTINEZ CERVANTES"},{"r_posicion_xy":"19.5394785,-99.1924916","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"7","folio_p":"7","lectura_anterior":"15007","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5393256,-99.1923561","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"8","folio_p":"8","lectura_anterior":"15008","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "}],"rastreos":[{"id":"1","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5393256,-99.1923561","rutas_id":"1"},{"id":"2","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5393559,-99.1923186","rutas_id":"1"},{"id":"3","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5394791,-99.1923367","rutas_id":"1"},{"id":"4","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5394943,-99.1924306","rutas_id":"1"},{"id":"5","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5395,-99.1925137","rutas_id":"1"},{"id":"6","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5395069,-99.1925962","rutas_id":"1"}],"equipos":[{"id":"1","modelo":"001","serie":"A001","descripcion":"Test 001","rutas_id":"1","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
        break;
      default:

    }
    var cmd = clbk+'('+data.toString()+')';
    eval(cmd);
  }, false);



  App.fireEvent = function(fName, Param) {
    var event = new MessageEvent(fName, {
      'view': window,
      'bubbles': false,
      'cancelable': false,
      'data': Param
    });
    document.dispatchEvent(event);
  };

  /*console.log2 = console.log;
  console.log = function(msg){
    $('#dbg-msg').append(msg+"\n");
  };
  $('#dbg-cmd').on('keypress',function(e){
    if(e.which === 13){
      var cmd = $('#dbg-cmd').val();
      eval(cmd);
    }
  });*/

})();

(function() {
  'use strict';

  App.Map = Backbone.View.extend({
    id: 'pMap',
    tagName: 'div',
    className: 'map-tools',
    map: undefined,
    markers: [],
    animOptions: {
      marker: undefined,
      delay: 50,
      speed: 800,
      coords: [],
      target: 0,
      targetMOV: 0,
      markStatus: [],
      planPoints: [],
      lineTrack: undefined,
      bounds: undefined,
      urlIcon: window.location.href.replace('index.html','').replace('#','').replace('!','')
    },
    template: window.templates.mapTools,
    initialize: function() {},
    activate: function() {
      var self = this;
      //var domElement = this.$('#pMap');
      this.map = new google.maps.Map($('#map')[0], {
        styles: window.mapStyle,
        center: {
          lat: 19.4342,
          lng: -99.1379
        },
        zoom: 11,
        zoomControl: false,
        streetViewControl: false,
        mapTypeControl: false,
      });
      //events
      google.maps.event.addListener(this.map, 'mousemove', this.displayCoordinates);

      var input = document.getElementById('searchMap');
      this.searchBox = new google.maps.places.SearchBox(input);

      this.searchBox.addListener('places_changed', function() {
        var places = self.searchBox.getPlaces();

        if (places.length === 0) {
          return;
        }

        // Clear out the old markers.
        self.markers.forEach(function(marker) {
          marker.setMap(null);
        });
        self.markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          self.markers.push(new google.maps.Marker({
            map: self.map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        self.map.fitBounds(bounds);
      });
    },
    render: function() {
      var htmlOutput = this.template();
      this.$el.html(htmlOutput);
      return this;
    },
    events: {
      'click #btnMapZoomIn': 'btnZoomIn',
      'click #btnMapZoomOut': 'btnZoomOut',
      'click .btnChangeLayer': 'changeLayer'
    },
    btnZoomIn: function() {
      this.map.setZoom(this.map.getZoom() + 1);
    },
    btnZoomOut: function() {
      this.map.setZoom(this.map.getZoom() - 1);
    },
    changeLayer: function(e) {
      var $b = $(e.target.tagName === 'I' ? e.target.parentNode : e.target);
      this.map.setMapTypeId(google.maps.MapTypeId[$b.data('layer')]);
    },
    displayCoordinates: function(event) {
      var pnt = event.latLng;
      var lat = pnt.lat();
      lat = lat.toFixed(4);
      var lng = pnt.lng();
      lng = lng.toFixed(4);
      $('#coords-tool').html("lat: " + lat + " lng: " + lng);
    },

    goToPoint: function(context) {
      var lat = context.animOptions.marker.position.lat();
      var lng = context.animOptions.marker.position.lng();
      var step = (context.animOptions.speed * 1000 * context.animOptions.delay) / 3600000; // in meters

      var dest = new google.maps.LatLng(
        context.animOptions.coordsMOV[context.animOptions.targetMOV][0], context.animOptions.coordsMOV[context.animOptions.targetMOV][1]);

      var distance =
        google.maps.geometry.spherical.computeDistanceBetween(
          dest, context.animOptions.marker.position);

      var numStep = distance / step;
      var i = 0;
      var deltaLat = (context.animOptions.coordsMOV[context.animOptions.targetMOV][0] - lat) / numStep;
      var deltaLng = (context.animOptions.coordsMOV[context.animOptions.targetMOV][1] - lng) / numStep;

      function moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;

        if (i < distance) {
          context.animOptions.marker.setPosition(new google.maps.LatLng(lat, lng));
          setTimeout(moveMarker, context.animOptions.delay);
        } else {
          context.animOptions.marker.setPosition(dest);
          context.animOptions.targetMOV++;
          if (context.animOptions.targetMOV === context.animOptions.coordsMOV.length) {
            context.animOptions.targetMOV = 0;
          } else {
            setTimeout(function() {
              context.goToPoint(context);
            }, context.animOptions.delay);
          }
        }
      }
      moveMarker();
    },
    animateMarker: function(coords) {
      var self = this;
      self.animOptions.coords = coords;

      self.animOptions.markStatus.forEach(function(o){
        o.setMap(null);
      });
      self.animOptions.planPoints.forEach(function(o){
        o.setMap(null);
      });

      $('#infoCont').html('');
      self.animOptions.markStatus = [];
      self.animOptions.planPoints = [];
      self.animOptions.bounds = new google.maps.LatLngBounds();
      if (self.animOptions.marker !== undefined) {
        self.animOptions.marker.setMap(null);
      }
      $('.layer-map').prop('checked',false);
      $('.layer-map').prop('disabled',false);
      if(coords.length === 0){
        $('.layer-map').prop('disabled',true);
      }

      //################################################
      //Planeados
      _.each(coords,function(obj){
        var o = obj[3];
        var coords = o.r_posicion_xy.split(',');
        if(coords.length!==2){
          return;
        }
        var icon = self.animOptions.urlIcon + '/images/s1.png';
        var pnt = new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1]));
        var marker = new google.maps.Marker({
          position: pnt,
          icon: icon,
          map: self.map,
          title: 'Lectura anterior: '+o.lectura_anterior
        });
        self.animOptions.planPoints.push(marker);
        self.animOptions.bounds.extend(pnt);
      });

      //################################################
      //Lecturas
      self.animOptions.coordsMOV = [];
      self.animOptions.target = 0;
      _.each(coords,function(obj){
        var o = obj[3];
        var coords = o.l_posicion_xy.split(',');
        if(coords.length!==2){
          return;
        }

        var inf = self.genInfoPopUp(o);
        var icon = self.animOptions.urlIcon + '/images/s'+obj[2]+'.png';
        $('#infoCont').append('<a data-id="'+self.animOptions.target+'" href="#" class="collection-item waves-effect waves-light item-marker"><img class="img-status" src="'+icon+'">'+inf.itemHtml+'</a>');
        var infowindow = new google.maps.InfoWindow({
          content: inf.infoHtml
        });
        self.animOptions.target++;


        var pnt = new google.maps.LatLng(coords[0], coords[1]);

        self.animOptions.coordsMOV.push([coords[0], coords[1]]);

        var marker = new google.maps.Marker({
          position: pnt,
          icon: icon,
          map: self.map,
          title: 'Estatus: '
        });

        marker.infowindow = infowindow;
        marker.addListener('click', function() {
          marker.infowindow.open(self.map, marker);
        });

        self.animOptions.markStatus.push(marker);
        self.animOptions.bounds.extend(pnt);
      });

      if(self.animOptions.markStatus.length>0 || self.animOptions.planPoints.length>0){
        self.map.fitBounds(self.animOptions.bounds);
        $('.layer-map').prop('checked',true);
      }

      if(self.animOptions.coordsMOV.length>0){
        var pos = new google.maps.LatLng(
          self.animOptions.coordsMOV[0][0],
          self.animOptions.coordsMOV[0][1]);
        if (self.animOptions.marker === undefined) {
          var icon = self.animOptions.urlIcon + '/images/walk.png';
          self.animOptions.marker = new google.maps.Marker({
            position: pos,
            map: self.map,
            icon: icon
          });
        }else{
          self.animOptions.marker.setPosition( pos );
        }
        if(self.animOptions.marker.getMap()===null){
          self.animOptions.marker.setMap(self.map);
        }
        pos = new google.maps.LatLng(
          self.animOptions.coordsMOV[self.animOptions.coordsMOV.length-1][0],
          self.animOptions.coordsMOV[self.animOptions.coordsMOV.length-1][1]);
        self.animOptions.targetMOV = 0;
        self.goToPoint(self);
      }
    },
    zoomToMarker: function(id){
      var self = this;
      _.each(this.animOptions.markStatus,function(marker,idx){
        marker.infowindow.close();
        if(idx===id){
          var pos = marker.getPosition();
          marker.infowindow.open(self.map, marker);
          self.map.panTo( pos );
        }
      });
    },
    //Rastreos
    addTracking: function(tracks){
      var self = this;
      if(self.animOptions.lineTrack !== undefined){
        self.animOptions.lineTrack.setMap(null);
        self.animOptions.lineTrack = undefined;
      }
      var dd = _.sortBy(tracks, function(o){
        return moment(o.fecha, 'DD/MM/YYYY HH:mm:ss');
      });
      var points = _.map(dd, function(o){
        var coords = o.posicion_xy.split(',');
        return {
          "lat":parseFloat(coords[0]),
          "lng":parseFloat(coords[1])
        };
      });
      /*[
        {lat: 37.772, lng: -122.214},
        {lat: 21.291, lng: -157.821},
        {lat: -18.142, lng: 178.431},
        {lat: -27.467, lng: 153.027}
      ];*/

      self.animOptions.lineTrack = new google.maps.Polyline({
        path: points,
        geodesic: true,
        strokeColor: '#FFAB00',
        strokeOpacity: 1.0,
        strokeWeight: 3
      });
      self.animOptions.lineTrack.setMap(self.map);
      //self.zoomToObjec(self.animOptions.lineTrack);
    },

    zoomToObjec: function(obj){
      var bounds = new google.maps.LatLngBounds();
      var points = obj.getPath().getArray();
      for (var n = 0; n < points.length ; n++){
        bounds.extend(points[n]);
      }
      this.map.fitBounds(bounds);
    },

    genInfoPopUp: function(info){
      var itemHtml = "";
      var infoHtml = "";
      var obs = "";
      infoHtml = '<div class="infopop">';
      infoHtml += '<div class="row">';
      infoHtml += '<div class="infopop-box col s8">';
      infoHtml += '<div class="infopop-fecha">'+info.fecha+'</div>';
      infoHtml += '<div class="infopop-folio">Folio: '+info.folio_l+'</div>';
      if(info.tipo==='2'){ //Incidencia
        itemHtml = "Incidencia: "+info.incidencia+"<br/>Lectura anterior: "+info.lectura_anterior;
        infoHtml += '<div class="infopop-incidencia">'+info.incidencia+'</div>';
        obs = "Incidencia: "+info.incidencia;
      }else{
        itemHtml = "Lectura: "+(info.lectura || 'Sin lectura')+"<br/>Lectura anterior: "+info.lectura_anterior;
        infoHtml += '<div class="infopop-lectura">Lectura: '+info.lectura+'</div>';
        obs = "Observaciones: "+info.observaciones;
      }
      infoHtml += '<div class="infopop-lectura-ant">Lectura anterior: '+info.lectura_anterior+'</div>';
      infoHtml += '<div class="infopop-telefono">Telefono: '+info.telefono+'</div>';
      infoHtml += '</div>';
      infoHtml += '<div class="infopop-img col s4"><img src="'+App.imgPath+info.ruta+'/'+info.nombre_img+App.imgExt+'"></div>';
      infoHtml += '</div>';

      infoHtml += '<div class="infopop-box2 row">';
      infoHtml += '<div class="infopop-direccion">Dirección: '+info.direccion+'</div>';
      infoHtml += '<div class="infopop-observacion">'+obs+'</div>';
      infoHtml += '</div>';

      infoHtml += '</div>';
      return {
        infoHtml:infoHtml,
        itemHtml:itemHtml,
        obs:obs
      };
    }
  });

})();

//https://snazzymaps.com/style/45666/bq-dark-teal
window.mapStyle = [{
  "featureType": "all",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#333333"
  }]
}, {
  "featureType": "all",
  "elementType": "geometry.stroke",
  "stylers": [{
    "color": "#31a392"
  }]
}, {
  "featureType": "all",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#00ebff"
  }]
}, {
  "featureType": "all",
  "elementType": "labels.text.stroke",
  "stylers": [{
    "color": "#333333"
  }]
}, {
  "featureType": "administrative",
  "elementType": "geometry",
  "stylers": [{
    "visibility": "on"
  }, {
    "color": "#333739"
  }, {
    "weight": 0.8
  }]
}, {
  "featureType": "administrative",
  "elementType": "geometry.stroke",
  "stylers": [{
    "color": "#ffa600"
  }]
}, {
  "featureType": "administrative",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#ffa600"
  }]
}, {
  "featureType": "landscape",
  "elementType": "geometry",
  "stylers": [{
    "color": "#333333"
  }]
}, {
  "featureType": "landscape",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#ffa600"
  }]
}, {
  "featureType": "landscape.man_made",
  "elementType": "geometry",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "poi",
  "elementType": "geometry.fill",
  "stylers": [{
    "visibility": "on"
  }, {
    "color": "#404040"
  }]
}, {
  "featureType": "poi",
  "elementType": "labels.text",
  "stylers": [{
    "visibility": "on"
  }]
}, {
  "featureType": "poi",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#00ebff"
  }]
}, {
  "featureType": "poi",
  "elementType": "labels.icon",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "poi.business",
  "elementType": "labels",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "poi.business",
  "elementType": "labels.icon",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "road",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#222222"
  }]
}, {
  "featureType": "road",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#41ffe3"
  }]
}, {
  "featureType": "road",
  "elementType": "labels.text.stroke",
  "stylers": [{
    "color": "#222222"
  }]
}, {
  "featureType": "transit",
  "elementType": "geometry",
  "stylers": [{
    "lightness": -34
  }, {
    "visibility": "on"
  }, {
    "color": "#555555"
  }]
}, {
  "featureType": "transit",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#00c547"
  }]
}, {
  "featureType": "transit.line",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#00c547"
  }]
}, {
  "featureType": "transit.line",
  "elementType": "labels.text.stroke",
  "stylers": [{
    "color": "#222222"
  }]
}, {
  "featureType": "transit.station",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#444444"
  }]
}, {
  "featureType": "water",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#222222"
  }]
}, {
  "featureType": "water",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#ffa600"
  }]
}, {
  "featureType": "water",
  "elementType": "labels.text.stroke",
  "stylers": [{
    "color": "#222222"
  }]
}];

(function() {
  'use strict';

  App.Menu = Backbone.View.extend({
    id: 'pMenu',
    tagName: 'div',
    className: '',
    template: window.templates.menu,
    initialize: function() {
      console.log('initialize menu');
    },
    render: function() {
      console.log('render menu');
      var htmlOutput = this.template();
      this.$el.html(htmlOutput);
      return this;
    },
    activate: function() {
      /*this.$("#btnMenu").sideNav({
        menuWidth: 350
      });*/
      //$('.select-mat').material_select();
      App.fireEvent('getData','rutas/;App.menu.fillSelectRoutes');
    },
    events: {
      'change #selRutas': 'chgSelRuta',
      'click #btnMenu': 'clickMenu',
      'click .item-marker': 'clickOnItemMarker',
      'click .layer-map':'clickLayerCheck',
      'click .zoom-layer':'zoom2layer'
    },
    clickMenu: function(){
      $('#MenuSec').slideToggle({
        duration:400
      });
    },
    fillSelectRoutes: function(d){
      console.log(d);
      var json = d;
      if(typeof d === 'string'){
        json = JSON.parse(d);
      }
      //$('#selRutas').material_select('destroy');
      $('#selRutas').html('<option value="" disabled selected>Rutas</option>');
      _.each(json, function(o){
        $('#selRutas').append('<option value="'+o.id+'">'+o.descripcion+' - '+o.fecha+'</option>');
      });
      //$('#selRutas').material_select();
    },

    chgSelRuta: function() {
      var id = $('#selRutas option:selected').val();
      App.fireEvent('getData','recorridosInfo/rutas_id='+id+';App.menu.setRecorridos');

      $('#layTrack').prop("checked", true);
      $('#layPlan').prop("checked", true);
      $('#layLect').prop("checked", true);
    },
    setRecorridos:function(d){
      var json = d;
      if(typeof d === 'string'){
        json = JSON.parse(d);
      }
      App.RUTA = json;
      console.log( d );
      var dd = _.sortBy(json.recorridos, function(o){
        return moment(o.fecha, 'DD/MM/YYYY HH:mm:ss');
      });
      var marks = _.map(dd,function(o){
        var coords = [];
        var tip = (o.tipo==='True'?2:3);
        if(o.l_posicion_xy !== ""){
          coords = o.l_posicion_xy.split(','); // cordenada de la lectura;
        }/*else if(o.r_posicion_xy !== ""){
          coords = o.r_posicion_xy.split(',');
          tip = 4;
        }*/
        if(o.l_posicion_xy === ""){
          tip = 4;
        }

        //return [parseFloat(o.latitud),parseFloat(o.longitud), (o.lectura.tipo==='True'?2:3), o ];
        return [parseFloat(coords[0]),parseFloat(coords[1]), tip, o ];
      });
      marks = _.filter(marks, function(o) {
        return o.length>0?true:false;
      });
      App.gmap.animateMarker(marks);
      App.gmap.addTracking(App.RUTA.rastreos);
    },
    clickOnItemMarker:function(e){
      //console.log(e);
      App.gmap.zoomToMarker($(e.target).data('id'));
    },
    clickLayerCheck:function(e){
      var $c = $(e.target);
      var isCheck = $c.is(':checked');
      switch($c.attr('id')){
        case 'layTrack':
          if(App.gmap.animOptions.lineTrack !== undefined){
            if(isCheck){
              App.gmap.animOptions.lineTrack.setMap(App.gmap.map);
            }else{
              App.gmap.animOptions.lineTrack.setMap(null);
            }
          }
        break;
        case 'layPlan':
          if(App.gmap.animOptions.planPoints.length > 0){
            App.gmap.animOptions.planPoints.forEach(function(o){
              if(isCheck){
                o.setMap(App.gmap.map);
              }else{
                o.setMap(null);
              }
            });
          }
        break;
        case 'layLect':
          if(App.gmap.animOptions.markStatus.length > 0){
            App.gmap.animOptions.markStatus.forEach(function(o){
              if(isCheck){
                o.setMap(App.gmap.map);
              }else{
                o.setMap(null);
              }
            });
          }
        break;
      }
    },

    zoom2layer:function(e){
      var $c = $(e.target);
      var bounds = new google.maps.LatLngBounds();
      switch($c.attr('id')){
        case 'zomTrack':
          if( App.gmap.animOptions.lineTrack && App.gmap.animOptions.lineTrack.getPath().length > 0){
            App.gmap.zoomToObjec(App.gmap.animOptions.lineTrack);
          }
        break;
        case 'zomPlan':
          _.each(App.gmap.animOptions.planPoints,function(o){
            bounds.extend( o.getPosition() );
          });
          if(App.gmap.animOptions.planPoints.length>0){
            App.gmap.map.fitBounds(bounds);
          }
        break;
        case 'zomLect':
          _.each(App.gmap.animOptions.markStatus,function(o){
            bounds.extend( o.getPosition() );
          });
          if(App.gmap.animOptions.markStatus.length>0){
            App.gmap.map.fitBounds(bounds);
          }
        break;
      }

    }
  });

})();
