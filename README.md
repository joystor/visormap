# Visor de rutas


## Instalación

Para instalar el proyecto es necesario el uso de node.js, una vez instalado, ejecutar los siguientes comandos, para instalar los paquetes necesarios del proyecto.

```
$ npm install
```


## Ejecución

Ejecutando el comando siguiente:

```
$ gulp dev
```

Se copilara el codigo fuente y abrira un navegar predeterminado del sistema, donde se visualizara el proyecto.

## Copilación

El sigueinte comando contruira una version del proyecto con archivos necesarios para distribuirlos en la a nivel productivo.

```
$ gulp build
```


## Estructura del proyecto

```
.
├── app           <-- Proyecto copilado para distribuir
│   ├── css
│   └── js
├── node_modules  <-- Paquetes para contruir y ejecutar el proyecto (no necesarios para la apliacion de distribución)
│   ├── *
└── src           <-- Codigo fuente
    ├── app         <-- Codigo JS Backbone
    └── styles      <-- Estilos Stylus (CSS)
```
