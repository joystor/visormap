var App = {};

App.imgPath = "http://joystor.xyz/media/";
App.imgExt = ".png";

(function() {
  'use strict';

  App.init = function() {
    this.gmap = new App.Map();
    $('#app').append(this.gmap.render().$el);
    this.gmap.activate();

    this.menu = new App.Menu();
    $('#app').append(this.menu.render().$el);
    this.menu.activate();

    this.RUTA = {};
  };

  $(function() {
    App.init();
  });

})();
