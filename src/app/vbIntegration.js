(function() {
  'use strict';

  /*
  3 check capas
  lineas naranja
  pines azul
  recorridos -pines a visitar azul
  lecturas - pines color verde e incidencias rojas
  rastreo - ruta naranja
  */
/*
  document.addEventListener('getData',function(e){
    var prms = e.data.split(';');
    var clbk = prms[1];
    var tbl = prms[0].split('/');
    var data = "[]";

    switch (tbl[0]) {
      case 'rutas':
        data = '[{"id":"1","descripcion":"GU0101","fecha":"11/11/2016 12:00:00 a.m."},{"id":"2","descripcion":"GU0102","fecha":"12/11/2016 12:00:00 a.m."},{"id":"3","descripcion":"GU0103","fecha":"13/11/2016 12:00:00 a.m."},{"id":"4","descripcion":"GU0104","fecha":"14/11/2016 12:00:00 a.m."},{"id":"5","descripcion":"GU0105","fecha":"15/11/2016 12:00:00 a.m."},{"id":"6","descripcion":"GU0106","fecha":"16/11/2016 12:00:00 a.m."},{"id":"7","descripcion":"GU0107","fecha":"17/11/2016 12:00:00 a.m."}]';
        break;
      case 'recorridosInfo_org':
        data = '{ '+ //6 5 2 3 4 7
'"recorridos":[ '+  //planeado r_posicion_xy
'  {"r_posicion_xy":"-99.194156,19.536871","lectura_id":"1","fecha":"22/10/2016 12:25","tipo":"1","folio_l":"2","lectura":"15002","incidencia":"","observaciones":"Observacion 1","l_posicion_xy":"-99.194156,19.536871","nombre_img":"nombre img 1","ruta":"imagen/01.png","predio_id":"1","folio_p":"2","lectura_anterior":"1200","tipo_servicio":"1","direccion":"dir 1","telefono":"000 000000","nombre":"juan","apellidos":"peres"}, '+
'  {"r_posicion_xy":"-99.194107,19.537255","lectura_id":"2","fecha":"22/10/2016 13:25","tipo":"2","folio_l":"3","lectura":"15245","incidencia":"Una incidencia 1","observaciones":"","l_posicion_xy":"-99.194107,19.537255","nombre_img":"nombre img 2","ruta":"imagen/01.png","predio_id":"2","folio_p":"3","lectura_anterior":"1200","tipo_servicio":"1","direccion":"dir2","telefono":"000 000000","nombre":"asd","apellidos":"ape"}, '+
'  {"r_posicion_xy":"-99.194071,19.537811","lectura_id":"3","fecha":"22/10/2016 14:25","tipo":"1","folio_l":"4","lectura":"15445","incidencia":"","observaciones":"Observacion 2","l_posicion_xy":"-99.194071,19.537811","nombre_img":"nombre img 3","ruta":"imagen/01.png","predio_id":"3","folio_p":"4","lectura_anterior":"1200","tipo_servicio":"2","direccion":"dir3","telefono":"000 000000","nombre":"qwerty","apellidos":"ape"}, '+
'  {"r_posicion_xy":"-99.193927,19.538029","lectura_id":"4","fecha":"22/10/2016 15:25","tipo":"2","folio_l":"5","lectura":"15645","incidencia":"Una incidencia 2","observaciones":"","l_posicion_xy":"-99.193927,19.538029","nombre_img":"nombre img 4","ruta":"imagen/01.png","predio_id":"4","folio_p":"5","lectura_anterior":"1200","tipo_servicio":"2","direccion":"dir4","telefono":"000 000000","nombre":"zxcvb","apellidos":"ape"}, '+
'  {"r_posicion_xy":"-99.193066,19.537901","lectura_id":"5","fecha":"22/10/2016 16:25","tipo":"1","folio_l":"6","lectura":"15845","incidencia":"","observaciones":"Observacion 3","l_posicion_xy":"-99.193066,19.537901","nombre_img":"nombre img 5","ruta":"imagen/01.png","predio_id":"5","folio_p":"6","lectura_anterior":"1200","tipo_servicio":"1","direccion":"dir5","telefono":"000 000000","nombre":"rtiy","apellidos":"ape"}, '+
'  {"r_posicion_xy":"-99.192792,19.539110","lectura_id":"6","fecha":"22/10/2016 17:25","tipo":"2","folio_l":"7","lectura":"15945","incidencia":"Una incidencia 3","observaciones":"","l_posicion_xy":"-99.192792,19.539110","nombre_img":"nombre img 6","ruta":"imagen/01.png","predio_id":"6","folio_p":"7","lectura_anterior":"1200","tipo_servicio":"1","direccion":"dir6","telefono":"000 000000","nombre":"uiopiup","apellidos":"ape"} '+
'], '+
'"rastreos":[ '+
'  {"id":"1","fecha":"22/10/2016 12:25","posicion_xy":"-99.194156,19.536871","rutas_id":"1"}, '+
'  {"id":"2","fecha":"22/10/2016 13:25","posicion_xy":"-99.194107,19.537255","rutas_id":"1"}, '+
'  {"id":"3","fecha":"22/10/2016 14:25","posicion_xy":"-99.194071,19.537811","rutas_id":"1"}, '+
'  {"id":"4","fecha":"22/10/2016 15:25","posicion_xy":"-99.193927,19.538029","rutas_id":"1"}, '+
'  {"id":"5","fecha":"22/10/2016 16:25","posicion_xy":"-99.193066,19.537901","rutas_id":"1"}, '+
'  {"id":"6","fecha":"22/10/2016 17:25","posicion_xy":"-99.192792,19.539110","rutas_id":"1"} '+
'], '+
'"equipos":[ '+
'  {"id":"","modelo":"","serie":"","descripcion":"","rutas_id":"","fecha_creacion":"","fecha_actualizacion":""} '+
']}';
        break;
      case 'recorridosInfo':
        var id = tbl[1].split('=')[1];
        switch(id){
          case '1':
          data = '{"recorridos":[{"r_posicion_xy":"19.5389407,-99.1928041","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img001","ruta":"img/lecturas","predio_id":"2","folio_p":"2","lectura_anterior":"15002","tipo_servicio":"True","direccion":"CARRETERA QUERETARO-SAN LUIS POTOSI No. 12401, Edif. ANTEA LIFESTYLE CENTER, Col. EJIDO EL SALITRE","telefono":"55-36-07-86","nombre":"F/1446","apellidos":"IXE BANCO, S.A."},{"r_posicion_xy":"19.5389407,-99.1928041","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img002","ruta":"img/lecturas","predio_id":"2","folio_p":"2","lectura_anterior":"15002","tipo_servicio":"True","direccion":"CARRETERA QUERETARO-SAN LUIS POTOSI No. 12401, Edif. ANTEA LIFESTYLE CENTER, Col. EJIDO EL SALITRE","telefono":"55-36-07-86","nombre":"F/1446","apellidos":"IXE BANCO, S.A."},{"r_posicion_xy":"19.5390722,-99.1928128","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"3","folio_p":"3","lectura_anterior":"15003","tipo_servicio":"True","direccion":"CIRCUITO BALCONES No. 228, Mz. 04, Col. BALCONES","telefono":"55-36-07-86","nombre":"MARIA MONICA","apellidos":"NUÑEZ RUBIO"},{"r_posicion_xy":"19.5392706,-99.1928108","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"4","folio_p":"4","lectura_anterior":"15004","tipo_servicio":"True","direccion":"ANILLO VIAL II \'FRAY JUNIPERO SERRA\' No. 2501, Mz. 02, Int. 07, Edif. EL RESPIRO, Col. SANTA FE","telefono":"55-36-07-86","nombre":"YOLANDA AIDA","apellidos":"CEJA LOPEZ"},{"r_posicion_xy":"19.5394791,-99.1927725","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"5","folio_p":"5","lectura_anterior":"15005","tipo_servicio":"True","direccion":"AV. SANTA ROSA No. 5201, Mz. 03, Int. 02, Edif. AURA, Col. VALLE DE JURIQUILLA","telefono":"55-36-07-86","nombre":"JUAN MANUEL","apellidos":"GONZALEZ RODRIGUEZ"},{"r_posicion_xy":"19.5394918,-99.192617","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"6","folio_p":"6","lectura_anterior":"15006","tipo_servicio":"True","direccion":"ANILLO VIAL II \'FRAY JUNIPERO SERRA\' No. 2501, Mz. 02, Int. 53, Edif. EL RESPIRO, Col. SANTA FE","telefono":"55-36-07-86","nombre":"GERARDO","apellidos":"MARTINEZ CERVANTES"},{"r_posicion_xy":"19.5394785,-99.1924916","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"7","folio_p":"7","lectura_anterior":"15007","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5393256,-99.1923561","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"8","folio_p":"8","lectura_anterior":"15008","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "}],"rastreos":[{"id":"1","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5393256,-99.1923561","rutas_id":"1"},{"id":"2","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5393559,-99.1923186","rutas_id":"1"},{"id":"3","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5394791,-99.1923367","rutas_id":"1"},{"id":"4","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5394943,-99.1924306","rutas_id":"1"},{"id":"5","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5395,-99.1925137","rutas_id":"1"},{"id":"6","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5395069,-99.1925962","rutas_id":"1"}],"equipos":[{"id":"1","modelo":"001","serie":"A001","descripcion":"Test 001","rutas_id":"1","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '2':
          data = '{"recorridos":[{"r_posicion_xy":"19.5418306,-99.19601","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"9","folio_p":"9","lectura_anterior":"15009","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5418306,-99.1955835","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"10","folio_p":"10","lectura_anterior":"15010","tipo_servicio":"True","direccion":"SANTA  ANA No. 116, Mz. 05, Col. VALLE DE JURIQUILLA","telefono":"55-36-07-86","nombre":"FERNANDO","apellidos":"RIVERA MENDOZA"},{"r_posicion_xy":"19.5418331,-99.1951999","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"11","folio_p":"11","lectura_anterior":"15011","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5418154,-99.1947279","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"12","folio_p":"12","lectura_anterior":"15012","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5417522,-99.1943685","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"13","folio_p":"13","lectura_anterior":"15013","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5417547,-99.1940949","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"14","folio_p":"14","lectura_anterior":"15014","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5417598,-99.1937247","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"15","folio_p":"15","lectura_anterior":"15015","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5417547,-99.1934914","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img001","ruta":"img/lecturas","predio_id":"16","folio_p":"16","lectura_anterior":"15016","tipo_servicio":"True","direccion":"CIRCUITO BALCONES No. 164, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"MANUEL GERARDO","apellidos":"GARCIA RANGEL"},{"r_posicion_xy":"19.5417547,-99.1934914","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img002","ruta":"img/lecturas","predio_id":"16","folio_p":"16","lectura_anterior":"15016","tipo_servicio":"True","direccion":"CIRCUITO BALCONES No. 164, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"MANUEL GERARDO","apellidos":"GARCIA RANGEL"}],"rastreos":[],"equipos":[{"id":"2","modelo":"001","serie":"A002","descripcion":"Test 002","rutas_id":"2","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '3':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"3","modelo":"001","serie":"A003","descripcion":"Test 003","rutas_id":"3","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '4':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"4","modelo":"001","serie":"A004","descripcion":"Test 004","rutas_id":"4","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '5':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"5","modelo":"002","serie":"B005","descripcion":"Test 005","rutas_id":"5","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '6':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"6","modelo":"002","serie":"B006","descripcion":"Test 006 ","rutas_id":"6","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
          case '7':
          data = '{"recorridos":[],"rastreos":[],"equipos":[{"id":"7","modelo":"002","serie":"B007","descripcion":"Test 007","rutas_id":"7","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
          break;
        }
        //data = '{"recorridos":[{"r_posicion_xy":"19.5389407,-99.1928041","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img001","ruta":"img/lecturas","predio_id":"2","folio_p":"2","lectura_anterior":"15002","tipo_servicio":"True","direccion":"CARRETERA QUERETARO-SAN LUIS POTOSI No. 12401, Edif. ANTEA LIFESTYLE CENTER, Col. EJIDO EL SALITRE","telefono":"55-36-07-86","nombre":"F/1446","apellidos":"IXE BANCO, S.A."},{"r_posicion_xy":"19.5389407,-99.1928041","lectura_id":"1","fecha":"10/11/2016 12:00:00 a.m.","tipo":"True","folio_l":"ABC01","lectura":"1234","incidencia":"N/A","observaciones":"Medidor 01","l_posicion_xy":"19.5417547,-99.1934914","nombre_img":"Img002","ruta":"img/lecturas","predio_id":"2","folio_p":"2","lectura_anterior":"15002","tipo_servicio":"True","direccion":"CARRETERA QUERETARO-SAN LUIS POTOSI No. 12401, Edif. ANTEA LIFESTYLE CENTER, Col. EJIDO EL SALITRE","telefono":"55-36-07-86","nombre":"F/1446","apellidos":"IXE BANCO, S.A."},{"r_posicion_xy":"19.5390722,-99.1928128","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"3","folio_p":"3","lectura_anterior":"15003","tipo_servicio":"True","direccion":"CIRCUITO BALCONES No. 228, Mz. 04, Col. BALCONES","telefono":"55-36-07-86","nombre":"MARIA MONICA","apellidos":"NUÑEZ RUBIO"},{"r_posicion_xy":"19.5392706,-99.1928108","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"4","folio_p":"4","lectura_anterior":"15004","tipo_servicio":"True","direccion":"ANILLO VIAL II \'FRAY JUNIPERO SERRA\' No. 2501, Mz. 02, Int. 07, Edif. EL RESPIRO, Col. SANTA FE","telefono":"55-36-07-86","nombre":"YOLANDA AIDA","apellidos":"CEJA LOPEZ"},{"r_posicion_xy":"19.5394791,-99.1927725","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"5","folio_p":"5","lectura_anterior":"15005","tipo_servicio":"True","direccion":"AV. SANTA ROSA No. 5201, Mz. 03, Int. 02, Edif. AURA, Col. VALLE DE JURIQUILLA","telefono":"55-36-07-86","nombre":"JUAN MANUEL","apellidos":"GONZALEZ RODRIGUEZ"},{"r_posicion_xy":"19.5394918,-99.192617","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"6","folio_p":"6","lectura_anterior":"15006","tipo_servicio":"True","direccion":"ANILLO VIAL II \'FRAY JUNIPERO SERRA\' No. 2501, Mz. 02, Int. 53, Edif. EL RESPIRO, Col. SANTA FE","telefono":"55-36-07-86","nombre":"GERARDO","apellidos":"MARTINEZ CERVANTES"},{"r_posicion_xy":"19.5394785,-99.1924916","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"7","folio_p":"7","lectura_anterior":"15007","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "},{"r_posicion_xy":"19.5393256,-99.1923561","lectura_id":"","fecha":"","tipo":"","folio_l":"","lectura":"","incidencia":"","observaciones":"","l_posicion_xy":"","nombre_img":"","ruta":"","predio_id":"8","folio_p":"8","lectura_anterior":"15008","tipo_servicio":"True","direccion":"DOMICILIO CONOCIDO, Mz. 01, Col. BALCONES","telefono":"55-36-07-86","nombre":"Usuario de la toma","apellidos":" "}],"rastreos":[{"id":"1","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5393256,-99.1923561","rutas_id":"1"},{"id":"2","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5393559,-99.1923186","rutas_id":"1"},{"id":"3","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5394791,-99.1923367","rutas_id":"1"},{"id":"4","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5394943,-99.1924306","rutas_id":"1"},{"id":"5","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5395,-99.1925137","rutas_id":"1"},{"id":"6","fecha":"10/11/2016 12:00:00 a.m.","posicion_xy":"19.5395069,-99.1925962","rutas_id":"1"}],"equipos":[{"id":"1","modelo":"001","serie":"A001","descripcion":"Test 001","rutas_id":"1","fecha_creacion":"10/11/2016 12:00:00 a.m.","fecha_actualizacion":"10/11/2016 12:00:00 a.m."}]}';
        break;
      default:

    }
    var cmd = clbk+'('+data.toString()+')';
    eval(cmd);
  }, false);
*/


  App.fireEvent = function(fName, Param) {
    var event = new MessageEvent(fName, {
      'view': window,
      'bubbles': false,
      'cancelable': false,
      'data': Param
    });
    document.dispatchEvent(event);
  };

  /*console.log2 = console.log;
  console.log = function(msg){
    $('#dbg-msg').append(msg+"\n");
  };
  $('#dbg-cmd').on('keypress',function(e){
    if(e.which === 13){
      var cmd = $('#dbg-cmd').val();
      eval(cmd);
    }
  });*/

})();
