(function() {
  'use strict';

  App.Menu = Backbone.View.extend({
    id: 'pMenu',
    tagName: 'div',
    className: '',
    template: window.templates.menu,
    initialize: function() {
      console.log('initialize menu');
    },
    render: function() {
      console.log('render menu');
      var htmlOutput = this.template();
      this.$el.html(htmlOutput);
      return this;
    },
    activate: function() {
      /*this.$("#btnMenu").sideNav({
        menuWidth: 350
      });*/
      //$('.select-mat').material_select();
      App.fireEvent('getData','rutas/;App.menu.fillSelectRoutes');
    },
    events: {
      'change #selRutas': 'chgSelRuta',
      'click #btnMenu': 'clickMenu',
      'click .item-marker': 'clickOnItemMarker',
      'click .layer-map':'clickLayerCheck',
      'click .zoom-layer':'zoom2layer'
    },
    clickMenu: function(){
      $('#MenuSec').slideToggle({
        duration:400
      });
    },
    fillSelectRoutes: function(d){
      console.log(d);
      var json = d;
      if(typeof d === 'string'){
        json = JSON.parse(d);
      }
      //$('#selRutas').material_select('destroy');
      $('#selRutas').html('<option value="" disabled selected>Rutas</option>');
      _.each(json, function(o){
        $('#selRutas').append('<option value="'+o.id+'">'+o.descripcion+' - '+o.fecha+'</option>');
      });
      //$('#selRutas').material_select();
    },

    chgSelRuta: function() {
      var id = $('#selRutas option:selected').val();
      App.fireEvent('getData','recorridosInfo/rutas_id='+id+';App.menu.setRecorridos');

      $('#layTrack').prop("checked", true);
      $('#layPlan').prop("checked", true);
      $('#layLect').prop("checked", true);
    },
    setRecorridos:function(d){
      var json = d;
      if(typeof d === 'string'){
        json = JSON.parse(d);
      }
      App.RUTA = json;
      console.log( d );
      var dd = _.sortBy(json.recorridos, function(o){
        return moment(o.fecha, 'DD/MM/YYYY HH:mm:ss');
      });
      var marks = _.map(dd,function(o){
        var coords = [];
        var tip = (o.tipo==='True'?2:3);
        if(o.l_posicion_xy !== ""){
          coords = o.l_posicion_xy.split(','); // cordenada de la lectura;
        }/*else if(o.r_posicion_xy !== ""){
          coords = o.r_posicion_xy.split(',');
          tip = 4;
        }*/
        if(o.l_posicion_xy === ""){
          tip = 4;
        }

        //return [parseFloat(o.latitud),parseFloat(o.longitud), (o.lectura.tipo==='True'?2:3), o ];
        return [parseFloat(coords[0]),parseFloat(coords[1]), tip, o ];
      });
      marks = _.filter(marks, function(o) {
        return o.length>0?true:false;
      });
      App.gmap.animateMarker(marks);
      App.gmap.addTracking(App.RUTA.rastreos);
    },
    clickOnItemMarker:function(e){
      //console.log(e);
      App.gmap.zoomToMarker($(e.target).data('id'));
    },
    clickLayerCheck:function(e){
      var $c = $(e.target);
      var isCheck = $c.is(':checked');
      switch($c.attr('id')){
        case 'layTrack':
          if(App.gmap.animOptions.lineTrack !== undefined){
            if(isCheck){
              App.gmap.animOptions.lineTrack.setMap(App.gmap.map);
            }else{
              App.gmap.animOptions.lineTrack.setMap(null);
            }
          }
        break;
        case 'layPlan':
          if(App.gmap.animOptions.planPoints.length > 0){
            App.gmap.animOptions.planPoints.forEach(function(o){
              if(isCheck){
                o.setMap(App.gmap.map);
              }else{
                o.setMap(null);
              }
            });
          }
        break;
        case 'layLect':
          if(App.gmap.animOptions.markStatus.length > 0){
            App.gmap.animOptions.markStatus.forEach(function(o){
              if(isCheck){
                o.setMap(App.gmap.map);
              }else{
                o.setMap(null);
              }
            });
          }
        break;
      }
    },

    zoom2layer:function(e){
      var $c = $(e.target);
      var bounds = new google.maps.LatLngBounds();
      switch($c.attr('id')){
        case 'zomTrack':
          if( App.gmap.animOptions.lineTrack && App.gmap.animOptions.lineTrack.getPath().length > 0){
            App.gmap.zoomToObjec(App.gmap.animOptions.lineTrack);
          }
        break;
        case 'zomPlan':
          _.each(App.gmap.animOptions.planPoints,function(o){
            bounds.extend( o.getPosition() );
          });
          if(App.gmap.animOptions.planPoints.length>0){
            App.gmap.map.fitBounds(bounds);
          }
        break;
        case 'zomLect':
          _.each(App.gmap.animOptions.markStatus,function(o){
            bounds.extend( o.getPosition() );
          });
          if(App.gmap.animOptions.markStatus.length>0){
            App.gmap.map.fitBounds(bounds);
          }
        break;
      }

    }
  });

})();
