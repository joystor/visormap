//https://snazzymaps.com/style/45666/bq-dark-teal
window.mapStyle = [{
  "featureType": "all",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#333333"
  }]
}, {
  "featureType": "all",
  "elementType": "geometry.stroke",
  "stylers": [{
    "color": "#31a392"
  }]
}, {
  "featureType": "all",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#00ebff"
  }]
}, {
  "featureType": "all",
  "elementType": "labels.text.stroke",
  "stylers": [{
    "color": "#333333"
  }]
}, {
  "featureType": "administrative",
  "elementType": "geometry",
  "stylers": [{
    "visibility": "on"
  }, {
    "color": "#333739"
  }, {
    "weight": 0.8
  }]
}, {
  "featureType": "administrative",
  "elementType": "geometry.stroke",
  "stylers": [{
    "color": "#ffa600"
  }]
}, {
  "featureType": "administrative",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#ffa600"
  }]
}, {
  "featureType": "landscape",
  "elementType": "geometry",
  "stylers": [{
    "color": "#333333"
  }]
}, {
  "featureType": "landscape",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#ffa600"
  }]
}, {
  "featureType": "landscape.man_made",
  "elementType": "geometry",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "poi",
  "elementType": "geometry.fill",
  "stylers": [{
    "visibility": "on"
  }, {
    "color": "#404040"
  }]
}, {
  "featureType": "poi",
  "elementType": "labels.text",
  "stylers": [{
    "visibility": "on"
  }]
}, {
  "featureType": "poi",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#00ebff"
  }]
}, {
  "featureType": "poi",
  "elementType": "labels.icon",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "poi.business",
  "elementType": "labels",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "poi.business",
  "elementType": "labels.icon",
  "stylers": [{
    "visibility": "off"
  }]
}, {
  "featureType": "road",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#222222"
  }]
}, {
  "featureType": "road",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#41ffe3"
  }]
}, {
  "featureType": "road",
  "elementType": "labels.text.stroke",
  "stylers": [{
    "color": "#222222"
  }]
}, {
  "featureType": "transit",
  "elementType": "geometry",
  "stylers": [{
    "lightness": -34
  }, {
    "visibility": "on"
  }, {
    "color": "#555555"
  }]
}, {
  "featureType": "transit",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#00c547"
  }]
}, {
  "featureType": "transit.line",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#00c547"
  }]
}, {
  "featureType": "transit.line",
  "elementType": "labels.text.stroke",
  "stylers": [{
    "color": "#222222"
  }]
}, {
  "featureType": "transit.station",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#444444"
  }]
}, {
  "featureType": "water",
  "elementType": "geometry.fill",
  "stylers": [{
    "color": "#222222"
  }]
}, {
  "featureType": "water",
  "elementType": "labels.text.fill",
  "stylers": [{
    "color": "#ffa600"
  }]
}, {
  "featureType": "water",
  "elementType": "labels.text.stroke",
  "stylers": [{
    "color": "#222222"
  }]
}];
