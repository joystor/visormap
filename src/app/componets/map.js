(function() {
  'use strict';

  App.Map = Backbone.View.extend({
    id: 'pMap',
    tagName: 'div',
    className: 'map-tools',
    map: undefined,
    markers: [],
    animOptions: {
      marker: undefined,
      delay: 50,
      speed: 800,
      coords: [],
      target: 0,
      targetMOV: 0,
      markStatus: [],
      planPoints: [],
      lineTrack: undefined,
      bounds: undefined,
      urlIcon: window.location.href.replace('index.html','').replace('#','').replace('!','')
    },
    template: window.templates.mapTools,
    initialize: function() {},
    activate: function() {
      var self = this;
      //var domElement = this.$('#pMap');
      this.map = new google.maps.Map($('#map')[0], {
        styles: window.mapStyle,
        center: {
          lat: 19.4342,
          lng: -99.1379
        },
        zoom: 11,
        zoomControl: false,
        streetViewControl: false,
        mapTypeControl: false,
      });
      //events
      google.maps.event.addListener(this.map, 'mousemove', this.displayCoordinates);

      var input = document.getElementById('searchMap');
      this.searchBox = new google.maps.places.SearchBox(input);

      this.searchBox.addListener('places_changed', function() {
        var places = self.searchBox.getPlaces();

        if (places.length === 0) {
          return;
        }

        // Clear out the old markers.
        self.markers.forEach(function(marker) {
          marker.setMap(null);
        });
        self.markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          self.markers.push(new google.maps.Marker({
            map: self.map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        self.map.fitBounds(bounds);
      });
    },
    render: function() {
      var htmlOutput = this.template();
      this.$el.html(htmlOutput);
      return this;
    },
    events: {
      'click #btnMapZoomIn': 'btnZoomIn',
      'click #btnMapZoomOut': 'btnZoomOut',
      'click .btnChangeLayer': 'changeLayer'
    },
    btnZoomIn: function() {
      this.map.setZoom(this.map.getZoom() + 1);
    },
    btnZoomOut: function() {
      this.map.setZoom(this.map.getZoom() - 1);
    },
    changeLayer: function(e) {
      var $b = $(e.target.tagName === 'I' ? e.target.parentNode : e.target);
      this.map.setMapTypeId(google.maps.MapTypeId[$b.data('layer')]);
    },
    displayCoordinates: function(event) {
      var pnt = event.latLng;
      var lat = pnt.lat();
      lat = lat.toFixed(4);
      var lng = pnt.lng();
      lng = lng.toFixed(4);
      $('#coords-tool').html("lat: " + lat + " lng: " + lng);
    },

    goToPoint: function(context) {
      var lat = context.animOptions.marker.position.lat();
      var lng = context.animOptions.marker.position.lng();
      var step = (context.animOptions.speed * 1000 * context.animOptions.delay) / 3600000; // in meters

      var dest = new google.maps.LatLng(
        context.animOptions.coordsMOV[context.animOptions.targetMOV][0], context.animOptions.coordsMOV[context.animOptions.targetMOV][1]);

      var distance =
        google.maps.geometry.spherical.computeDistanceBetween(
          dest, context.animOptions.marker.position);

      var numStep = distance / step;
      var i = 0;
      var deltaLat = (context.animOptions.coordsMOV[context.animOptions.targetMOV][0] - lat) / numStep;
      var deltaLng = (context.animOptions.coordsMOV[context.animOptions.targetMOV][1] - lng) / numStep;

      function moveMarker() {
        lat += deltaLat;
        lng += deltaLng;
        i += step;

        if (i < distance) {
          context.animOptions.marker.setPosition(new google.maps.LatLng(lat, lng));
          setTimeout(moveMarker, context.animOptions.delay);
        } else {
          context.animOptions.marker.setPosition(dest);
          context.animOptions.targetMOV++;
          if (context.animOptions.targetMOV === context.animOptions.coordsMOV.length) {
            context.animOptions.targetMOV = 0;
          } else {
            setTimeout(function() {
              context.goToPoint(context);
            }, context.animOptions.delay);
          }
        }
      }
      moveMarker();
    },
    animateMarker: function(coords) {
      var self = this;
      self.animOptions.coords = coords;

      self.animOptions.markStatus.forEach(function(o){
        o.setMap(null);
      });
      self.animOptions.planPoints.forEach(function(o){
        o.setMap(null);
      });

      $('#infoCont').html('');
      self.animOptions.markStatus = [];
      self.animOptions.planPoints = [];
      self.animOptions.bounds = new google.maps.LatLngBounds();
      if (self.animOptions.marker !== undefined) {
        self.animOptions.marker.setMap(null);
      }
      $('.layer-map').prop('checked',false);
      $('.layer-map').prop('disabled',false);
      if(coords.length === 0){
        $('.layer-map').prop('disabled',true);
      }

      //################################################
      //Planeados
      _.each(coords,function(obj){
        var o = obj[3];
        var coords = o.r_posicion_xy.split(',');
        if(coords.length!==2){
          return;
        }
        var icon = self.animOptions.urlIcon + '/images/s1.png';
        var pnt = new google.maps.LatLng(parseFloat(coords[0]), parseFloat(coords[1]));
        var marker = new google.maps.Marker({
          position: pnt,
          icon: icon,
          map: self.map,
          title: 'Lectura anterior: '+o.lectura_anterior
        });
        self.animOptions.planPoints.push(marker);
        self.animOptions.bounds.extend(pnt);
      });

      //################################################
      //Lecturas
      self.animOptions.coordsMOV = [];
      self.animOptions.target = 0;
      _.each(coords,function(obj){
        var o = obj[3];
        var coords = o.l_posicion_xy.split(',');
        if(coords.length!==2){
          return;
        }

        var inf = self.genInfoPopUp(o);
        var icon = self.animOptions.urlIcon + '/images/s'+obj[2]+'.png';
        $('#infoCont').append('<a data-id="'+self.animOptions.target+'" href="#" class="collection-item waves-effect waves-light item-marker"><img class="img-status" src="'+icon+'">'+inf.itemHtml+'</a>');
        var infowindow = new google.maps.InfoWindow({
          content: inf.infoHtml
        });
        self.animOptions.target++;


        var pnt = new google.maps.LatLng(coords[0], coords[1]);

        self.animOptions.coordsMOV.push([coords[0], coords[1]]);

        var marker = new google.maps.Marker({
          position: pnt,
          icon: icon,
          map: self.map,
          title: 'Estatus: '
        });

        marker.infowindow = infowindow;
        marker.addListener('click', function() {
          marker.infowindow.open(self.map, marker);
        });

        self.animOptions.markStatus.push(marker);
        self.animOptions.bounds.extend(pnt);
      });

      if(self.animOptions.markStatus.length>0 || self.animOptions.planPoints.length>0){
        self.map.fitBounds(self.animOptions.bounds);
        $('.layer-map').prop('checked',true);
      }

      if(self.animOptions.coordsMOV.length>0){
        var pos = new google.maps.LatLng(
          self.animOptions.coordsMOV[0][0],
          self.animOptions.coordsMOV[0][1]);
        if (self.animOptions.marker === undefined) {
          var icon = self.animOptions.urlIcon + '/images/walk.png';
          self.animOptions.marker = new google.maps.Marker({
            position: pos,
            map: self.map,
            icon: icon
          });
        }else{
          self.animOptions.marker.setPosition( pos );
        }
        if(self.animOptions.marker.getMap()===null){
          self.animOptions.marker.setMap(self.map);
        }
        pos = new google.maps.LatLng(
          self.animOptions.coordsMOV[self.animOptions.coordsMOV.length-1][0],
          self.animOptions.coordsMOV[self.animOptions.coordsMOV.length-1][1]);
        self.animOptions.targetMOV = 0;
        self.goToPoint(self);
      }
    },
    zoomToMarker: function(id){
      var self = this;
      _.each(this.animOptions.markStatus,function(marker,idx){
        marker.infowindow.close();
        if(idx===id){
          var pos = marker.getPosition();
          marker.infowindow.open(self.map, marker);
          self.map.panTo( pos );
        }
      });
    },
    //Rastreos
    addTracking: function(tracks){
      var self = this;
      if(self.animOptions.lineTrack !== undefined){
        self.animOptions.lineTrack.setMap(null);
        self.animOptions.lineTrack = undefined;
      }
      var dd = _.sortBy(tracks, function(o){
        return moment(o.fecha, 'DD/MM/YYYY HH:mm:ss');
      });
      var points = _.map(dd, function(o){
        var coords = o.posicion_xy.split(',');
        return {
          "lat":parseFloat(coords[0]),
          "lng":parseFloat(coords[1])
        };
      });
      /*[
        {lat: 37.772, lng: -122.214},
        {lat: 21.291, lng: -157.821},
        {lat: -18.142, lng: 178.431},
        {lat: -27.467, lng: 153.027}
      ];*/

      self.animOptions.lineTrack = new google.maps.Polyline({
        path: points,
        geodesic: true,
        strokeColor: '#FFAB00',
        strokeOpacity: 1.0,
        strokeWeight: 3
      });
      self.animOptions.lineTrack.setMap(self.map);
      //self.zoomToObjec(self.animOptions.lineTrack);
    },

    zoomToObjec: function(obj){
      var bounds = new google.maps.LatLngBounds();
      var points = obj.getPath().getArray();
      for (var n = 0; n < points.length ; n++){
        bounds.extend(points[n]);
      }
      this.map.fitBounds(bounds);
    },

    genInfoPopUp: function(info){
      var itemHtml = "";
      var infoHtml = "";
      var obs = "";
      infoHtml = '<div class="infopop">';
      infoHtml += '<div class="row">';
      infoHtml += '<div class="infopop-box col s8">';
      infoHtml += '<div class="infopop-fecha">'+info.fecha+'</div>';
      infoHtml += '<div class="infopop-folio">Folio: '+info.folio_l+'</div>';
      if(info.tipo==='2'){ //Incidencia
        itemHtml = "Incidencia: "+info.incidencia+"<br/>Lectura anterior: "+info.lectura_anterior;
        infoHtml += '<div class="infopop-incidencia">'+info.incidencia+'</div>';
        obs = "Incidencia: "+info.incidencia;
      }else{
        itemHtml = "Lectura: "+(info.lectura || 'Sin lectura')+"<br/>Lectura anterior: "+info.lectura_anterior;
        infoHtml += '<div class="infopop-lectura">Lectura: '+info.lectura+'</div>';
        obs = "Observaciones: "+info.observaciones;
      }
      infoHtml += '<div class="infopop-lectura-ant">Lectura anterior: '+info.lectura_anterior+'</div>';
      infoHtml += '<div class="infopop-telefono">Telefono: '+info.telefono+'</div>';
      infoHtml += '</div>';
      infoHtml += '<div class="infopop-img col s4"><img src="'+App.imgPath+info.ruta+'/'+info.nombre_img+App.imgExt+'"></div>';
      infoHtml += '</div>';

      infoHtml += '<div class="infopop-box2 row">';
      infoHtml += '<div class="infopop-direccion">Dirección: '+info.direccion+'</div>';
      infoHtml += '<div class="infopop-observacion">'+obs+'</div>';
      infoHtml += '</div>';

      infoHtml += '</div>';
      return {
        infoHtml:infoHtml,
        itemHtml:itemHtml,
        obs:obs
      };
    }
  });

})();
